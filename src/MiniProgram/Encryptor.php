<?php

namespace Baidu\MiniProgram;

use Baidu\Kernel\Encryptor as BaseEncryptor;
use HttpBase\Exceptions\DecryptException;

/**
 * Class Encryptor.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Encryptor extends BaseEncryptor
{
    /**
     * @param $appKey
     * @param string $sessionKey
     * @param string $iv
     * @param string $encrypted
     * @return false|string
     */
    public function decryptData(string $sessionKey, string $iv, string $encrypted)
    {
        $session_key = base64_decode($sessionKey);
        $iv = base64_decode($iv);
        $ciphertext = base64_decode($encrypted);

        $plaintext = openssl_decrypt($ciphertext, "AES-192-CBC", $session_key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv);
        $plaintext = $this->pkcs7Unpad($plaintext);
        // trim header
        $plaintext = substr($plaintext, 16);
        // get content length
        $unpack = unpack("Nlen/", substr($plaintext, 0, 4));
        // get content
        $content = substr($plaintext, 4, $unpack['len']);
        $decrypted = json_decode($content, true);

        if (!$decrypted) {
            throw new DecryptException('The given payload is invalid.');
        }

        return $decrypted;
    }
}
