<?php

namespace Baidu\MiniProgram\Auth;

use Baidu\Kernel\BaseClient;

class Client extends BaseClient
{

    /**
     * Get session info by code.
     * @param string $code
     * @return array|\Baidu\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function session(string $code)
    {
        $params = [
            'code' => $code,
            'grant_type' => 'authorization_code',
        ];

        return $this->httpGet('sns/jscode2session', $params);
    }
}
