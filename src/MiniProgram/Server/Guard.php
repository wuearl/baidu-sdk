<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Baidu\MiniProgram\Server;

use Baidu\Kernel\ServerGuard;
use Baidu\Kernel\Messages\Message;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Guard.
 *
 * @author overtrue <i@overtrue.me>
 */
class Guard extends ServerGuard
{
    const EVENT_MAPPING = [
        'PACKAGE_AUDIT_FAIL' => Message::EVENT,
        'PACKAGE_AUDIT_PASS' => Message::EVENT
    ];

    /**
     * @return Response
     * @throws \HttpBase\Exceptions\BadRequestException
     * @throws \HttpBase\Exceptions\InvalidArgumentException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    protected function resolve(): Response
    {
         $message = $this->getMessage();

         if (isset($message['event']) && array_key_exists($message['event'], static::EVENT_MAPPING)) {
             $this->dispatch(static::EVENT_MAPPING[$message['event']], $message);
         }
        return new Response(static::SUCCESS_EMPTY_RESPONSE);
     }

    /**
     * @return bool
     * @throws \HttpBase\Exceptions\BadRequestException
     * @throws \HttpBase\Exceptions\InvalidArgumentException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    protected function shouldReturnRawResponse(): bool
    {
        $message = $this->getMessage();

        return isset($message['event']) && array_key_exists($message['event'], static::EVENT_MAPPING);
    }
}
