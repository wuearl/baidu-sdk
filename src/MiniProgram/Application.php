<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Baidu\MiniProgram;

use Baidu\Kernel\ServiceContainer;
use Baidu\OpenPlatform\Authorizer\Auth\AccessToken;

/**
 * Class Application.
 *
 * @author brooke <overbob@yeah.net>
 * @property \Baidu\MiniProgram\Auth\AccessToken           $access_token
 * @property \Baidu\MiniProgram\Base\Client                $base
 * @property \Baidu\MiniProgram\Auth\Client                $auth
 * @property \Baidu\MiniProgram\File\Client                $file
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        Base\ServiceProvider::class,
        Auth\ServiceProvider::class,
        File\ServiceProvider::class,
        Server\ServiceProvider::class,
    ];

    /**
     * Handle dynamic calls.
     *
     * @param string $method
     * @param array $args
     *
     * @return mixed
     */
    public function __call($method, $args)
    {
        return $this->base->$method(...$args);
    }

    public function registerTokenHandlers(callable $callable)
    {
        $this->access_token->on(AccessToken::EVENT_ACCESS_TOKEN, $callable);
    }
}
