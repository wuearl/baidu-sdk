<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Baidu\Payment\Notify;

use Closure;
use HttpBase\Exceptions\Exception;
use Baidu\Kernel\Support;
use Baidu\Kernel\Support\XML;
use Baidu\Payment\Kernel\Exceptions\InvalidSignException;
use Symfony\Component\HttpFoundation\Response;

abstract class Handler
{
    const SUCCESS = 'SUCCESS';
    const FAIL = 'FAIL';

    /**
     * @var \Baidu\Payment\Application
     */
    protected $app;

    /**
     * @var array
     */
    protected $message;
    protected $data;

    /**
     * @var string|null
     */
    protected $fail;

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * Check sign.
     * If failed, throws an exception.
     *
     * @var bool
     */
    protected $check = true;

    /**
     * Respond with sign.
     *
     * @var bool
     */
    protected $sign = false;

    /**
     * @param \Baidu\Payment\Application $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Handle incoming notify.
     *
     * @param \Closure $closure
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    abstract public function handle(Closure $closure);

    /**
     * @param string $message
     */
    public function fail(string $message)
    {
        $this->fail = $message;
    }

    public function data(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param array $attributes
     * @param bool $sign
     *
     * @return $this
     */
    public function respondWith(array $attributes, bool $sign = false)
    {
        $this->attributes = $attributes;
        $this->sign = $sign;

        return $this;
    }

    /**
     * Build xml and return the response to WeChat.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \HttpBase\Exceptions\InvalidArgumentException
     */
    public function toResponse(): Response
    {
        $base = [
            'errno' => is_null($this->fail) ? 0 : 400,
            'msg' => is_null($this->fail) ? static::SUCCESS : static::FAIL,
            'data' => $this->data
        ];

        $attributes = array_merge($base, $this->attributes);
        return new Response(json_encode($attributes));
    }

    /**
     * Return the notify message from request.
     *
     * @return array
     *
     * @throws \HttpBase\Exceptions\Exception
     */
    public function getMessage(): array
    {
        if (!empty($this->message)) {
            return $this->message;
        }

        try {
            parse_str($this->app['request']->getContent(), $message);
        } catch (\Throwable $e) {
            throw new Exception('Invalid request XML: ' . $e->getMessage(), 400);
        }

        if (!is_array($message) || empty($message)) {
            throw new Exception('Invalid request XML.', 400);
        }

        if ($this->check) {
            $this->validate($message);
        }

        return $this->message = $message;
    }

    /**
     * Decrypt message.
     *
     * @param string $key
     *
     * @return string|null
     *
     * @throws \HttpBase\Exceptions\Exception
     */
    public function decryptMessage(string $key)
    {
        $message = $this->getMessage();
        if (empty($message[$key])) {
            return null;
        }

        return Support\AES::decrypt(
            base64_decode($message[$key], true),
            md5($this->app['config']->key),
            '',
            OPENSSL_RAW_DATA,
            'AES-256-ECB'
        );
    }

    /**
     * Validate the request params.
     *
     * @param array $message
     *
     * @throws \Baidu\Payment\Kernel\Exceptions\InvalidSignException
     * @throws \HttpBase\Exceptions\InvalidArgumentException
     */
    protected function validate(array $message)
    {
        $sign = $message['rsaSign'];
        unset($message['rsaSign']);

        if (Support\generate_verify($sign, $message, $this->app->config->get('public_key'))) {
            throw new InvalidSignException();
        }
    }

    /**
     * @param mixed $result
     */
    protected function strict($result)
    {
        if (true !== $result && is_null($this->fail)) {
            $this->fail(strval($result));
        }
    }
}
