<?php
/**
 * @desc Created by PhpStorm
 * @author: wuearl
 * @since: 2022/1/14 9:13 上午
 */

namespace Baidu\Payment\Refund;

use Baidu\Kernel\Support;
use Baidu\Payment\Kernel\BaseClient;

class Client extends BaseClient
{
    /**
     * @param string $tpOrderId
     * @param string $orderId
     * @param string $userId
     * @param array $options
     * @return array|Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function refund(string $tpOrderId, string $orderId, string $userId, array $options = [])
    {
        $data = array_merge([
            'userId' => $userId,
            'tpOrderId' => $tpOrderId,
            'orderId' => $orderId
        ], $options);
        return $this->httpPost('pay/paymentservice/applyOrderRefund', $data);
    }

    /**
     * 查询退款
     * @param $outOrderId
     * @param $userId
     * @return array|Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function findRefund($outOrderId, $userId)
    {
        return $this->httpGet('pay/paymentservice/applyOrderRefund', [
            'tpOrderId' => $outOrderId,
            'userId' => $userId
        ]);
    }
}