<?php
/**
 * @desc Created by PhpStorm
 * @author: wuearl
 * @since: 2022/1/14 9:13 上午
 */

namespace Baidu\Payment\Order;

use Baidu\Kernel\Support;
use Baidu\Payment\Kernel\BaseClient;

class Client extends BaseClient
{
    /**
     * @param $tpOrderId
     * @param $totalAmount
     * @return string
     * @throws \Baidu\Payment\Kernel\Exceptions\InvalidSignException
     */
    public function unify($tpOrderId, $totalAmount): string
    {
        $params = [
            'appKey' => $this->app->config->get('app_key'),
            'dealId' => $this->app->config->get('deal_id'),
            'tpOrderId' => $tpOrderId,
            'totalAmount' => $totalAmount,
        ];

        return Support\generate_sign($params, $this->app->config->get('private_key'));
    }

    /**
     * 查询订单
     * @param string $outOrderId
     * @return array|Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function findByTpOrderId(string $outOrderId)
    {
        return $this->httpGet('pay/paymentservice/findByTpOrderId', [
            'tpOrderId' => $outOrderId
        ]);
    }

    /**
     * 关闭订单
     * @param string $outOrderId
     * @return array|Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function cancelOrder(string $outOrderId)
    {
        return $this->httpGet('pay/paymentservice/cancelOrder', [
            'tpOrderId' => $outOrderId
        ]);
    }
}