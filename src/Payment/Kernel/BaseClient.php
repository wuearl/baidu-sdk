<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Baidu\Payment\Kernel;

use HttpBase\Traits\HasHttpRequests;
use Baidu\Payment\Application;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use Psr\Http\Message\ResponseInterface;
use Baidu\Kernel\BaseClient as Client;

/**
 * Class BaseClient.
 *
 * @author overtrue <i@overtrue.me>
 */
class BaseClient extends Client
{

    /**
     * GET request.
     *
     * @param string $url
     * @param array $query
     *
     * @return \Psr\Http\Message\ResponseInterface|\Baidu\Kernel\Support\Collection|array|object|string
     *
     * @throws \HttpBase\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function httpGet(string $url, array $query = [])
    {
        $query = array_merge([
            'pmAppKey' => $this->app->config->get('app_key'),
            'accessToken' => $this->app->config->get('access_token'),
        ], $query);
        return $this->request($url, 'GET', ['query' => $query]);
    }

    /**
     * POST request.
     *
     * @param string $url
     * @param array $data
     *
     * @return \Psr\Http\Message\ResponseInterface|\Baidu\Kernel\Support\Collection|array|object|string
     *
     * @throws \HttpBase\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function httpPost(string $url, array $data = [])
    {
        $data = array_merge([
            'pmAppKey' => $this->app->config->get('app_key')
        ], $data);
        return $this->request($url, 'POST', ['form_params' => $data,'query'=>['accessToken' => $this->app->config->get('access_token'),]]);
    }
}
