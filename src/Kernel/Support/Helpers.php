<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Baidu\Kernel\Support;

/*
 * helpers.
 *
 * @author overtrue <i@overtrue.me>
 */

use Baidu\Payment\Kernel\Exceptions\InvalidSignException;

/**
 * Generate a signature.
 * @param array $attributes
 * @param $privateKey
 * @return string
 * @throws InvalidSignException
 */
function generate_sign(array $attributes, $privateKey): string
{
    ksort($attributes);
    $data = urldecode(http_build_query($attributes));
    $privateKey = openssl_pkey_get_private(
        Str::startsWith($privateKey, 'file://') ? $privateKey : 'file://' . $privateKey
    );
    $result = openssl_sign($data, $sign, $privateKey, OPENSSL_ALGO_SHA1);
    if ($result === false) {
        throw new InvalidSignException();
    }
    if (is_resource($privateKey)) {
        openssl_free_key($privateKey);
    }
    return base64_encode($sign);
}

/**
 * @param $sign
 * @param $data
 * @param $publicKey
 * @return bool
 */
function generate_verify($sign, $data, $publicKey): bool
{
    ksort($data);
    $publicKey = openssl_pkey_get_public(
        Str::startsWith($publicKey, 'file://') ? $publicKey : 'file://' . $publicKey
    );
    $data = json_encode($data, JSON_UNESCAPED_UNICODE);
    $result = (openssl_verify($data, base64_decode($sign), $publicKey, OPENSSL_ALGO_SHA1) === 1);
    if (is_resource($publicKey)) {
        //释放资源
        openssl_free_key($publicKey);
    }
    return $result;
}

/**
 * @param string $signType
 * @param string $secretKey
 *
 * @return \Closure|string
 */
function get_encrypt_method(string $signType, string $secretKey = '')
{
    if ('HMAC-SHA256' === $signType) {
        return function ($str) use ($secretKey) {
            return hash_hmac('sha256', $str, $secretKey);
        };
    }

    return 'md5';
}

/**
 * Get client ip.
 *
 * @return string
 */
function get_client_ip()
{
    if (!empty($_SERVER['REMOTE_ADDR'])) {
        $ip = $_SERVER['REMOTE_ADDR'];
    } else {
        // for php-cli(phpunit etc.)
        $ip = defined('PHPUNIT_RUNNING') ? '127.0.0.1' : gethostbyname(gethostname());
    }

    return filter_var($ip, FILTER_VALIDATE_IP) ?: '127.0.0.1';
}

/**
 * Get current server ip.
 *
 * @return string
 */
function get_server_ip()
{
    if (!empty($_SERVER['SERVER_ADDR'])) {
        $ip = $_SERVER['SERVER_ADDR'];
    } elseif (!empty($_SERVER['SERVER_NAME'])) {
        $ip = gethostbyname($_SERVER['SERVER_NAME']);
    } else {
        // for php-cli(phpunit etc.)
        $ip = defined('PHPUNIT_RUNNING') ? '127.0.0.1' : gethostbyname(gethostname());
    }

    return filter_var($ip, FILTER_VALIDATE_IP) ?: '127.0.0.1';
}

/**
 * Return current url.
 *
 * @return string
 */
function current_url()
{
    $protocol = 'http://';

    if ((!empty($_SERVER['HTTPS']) && 'off' !== $_SERVER['HTTPS']) || ($_SERVER['HTTP_X_FORWARDED_PROTO'] ?? 'http') === 'https') {
        $protocol = 'https://';
    }

    return $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

/**
 * Return random string.
 *
 * @param string $length
 *
 * @return string
 */
function str_random($length)
{
    return Str::random($length);
}

/**
 * @param string $content
 * @param string $publicKey
 *
 * @return string
 */
function rsa_public_encrypt($content, $publicKey)
{
    $encrypted = '';
    openssl_public_encrypt($content, $encrypted, openssl_pkey_get_public($publicKey), OPENSSL_PKCS1_OAEP_PADDING);

    return base64_encode($encrypted);
}
