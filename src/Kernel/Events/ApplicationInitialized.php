<?php

namespace Baidu\Kernel\Events;

use Baidu\Kernel\ServiceContainer;

/**
 * Class ApplicationInitialized.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class ApplicationInitialized
{
    /**
     * @var \Baidu\Kernel\ServiceContainer
     */
    public $app;

    /**
     * @param \Baidu\Kernel\ServiceContainer $app
     */
    public function __construct(ServiceContainer $app)
    {
        $this->app = $app;
    }
}
