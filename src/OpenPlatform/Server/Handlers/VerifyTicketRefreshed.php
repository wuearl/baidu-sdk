<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Baidu\OpenPlatform\Server\Handlers;

use HttpBase\Contracts\EventHandlerInterface;
use Baidu\Kernel\ServiceContainer;

/**
 * Class VerifyTicketRefreshed.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class VerifyTicketRefreshed implements EventHandlerInterface
{
    /**
     * @var \Baidu\OpenPlatform\Application
     */
    protected $app;

    /**
     * Constructor.
     *
     * @param \Baidu\OpenPlatform\Application $app
     */
    public function __construct(ServiceContainer $app)
    {
        $this->app = $app;
    }

    /**
     * {@inheritdoc}.
     */
    public function handle($payload = null)
    {
        if (!empty($payload['Ticket'])) {
            $this->app['verify_ticket']->setTicket($payload['Ticket']);
        }
    }
}
