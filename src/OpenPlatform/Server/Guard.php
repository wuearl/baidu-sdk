<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Baidu\OpenPlatform\Server;

use Baidu\Kernel\ServerGuard;
use Baidu\OpenPlatform\Server\Handlers\Authorized;
use Baidu\OpenPlatform\Server\Handlers\Unauthorized;
use Baidu\OpenPlatform\Server\Handlers\UpdateAuthorized;
use Baidu\OpenPlatform\Server\Handlers\VerifyTicketRefreshed;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Guard.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Guard extends ServerGuard
{
    const EVENT_AUTHORIZED = 'AUTHORIZED';
    const EVENT_UNAUTHORIZED = 'UNAUTHORIZED';
    const EVENT_UPDATE_AUTHORIZED = 'UPDATE_AUTHORIZED';
    const EVENT_COMPONENT_VERIFY_TICKET = 'ticket';

    /**
     * @return Response
     * @throws \HttpBase\Exceptions\BadRequestException
     * @throws \HttpBase\Exceptions\InvalidArgumentException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    protected function resolve(): Response
    {
         $this->registerHandlers();

         $message = $this->getMessage();

         if (isset($message['MsgType'])) {
             $this->dispatch($message['MsgType'], $message);
         }else if(isset($message['event'])) {
             $this->dispatch($message['event'], $message);
         }

         return new Response(static::SUCCESS_EMPTY_RESPONSE);
     }

    /**
     * Register event handlers.
     */
    protected function registerHandlers()
    {
        $this->on(self::EVENT_AUTHORIZED, Authorized::class);
        $this->on(self::EVENT_UPDATE_AUTHORIZED, UpdateAuthorized::class);
        $this->on(self::EVENT_UNAUTHORIZED, Unauthorized::class);
        $this->on(self::EVENT_COMPONENT_VERIFY_TICKET, VerifyTicketRefreshed::class);
    }
}
