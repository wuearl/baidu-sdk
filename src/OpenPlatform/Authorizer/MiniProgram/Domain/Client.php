<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Baidu\OpenPlatform\Authorizer\MiniProgram\Domain;

use Baidu\Kernel\BaseClient;

/**
 * Class Client.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Client extends BaseClient
{
    /**
     * @param array $params
     * @param string $action
     * @return array|\Baidu\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function modify(array $params, $action = 'add')
    {
        $params = array_map(function ($domains) {
            return count($domains) > 0 ? implode(',', $domains) : '';
        }, $params);

        $params['action'] = $action;

        return $this->httpPostJson('rest/2.0/smartapp/app/modifydomain', $params);
    }

    /**
     * 设置小程序业务域名
     * @param array $domains
     * @param string $action
     * @return array|\Baidu\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function setWebviewDomain(array $domains, $action = 'add')
    {
        $domains = count($domains) > 0 ? implode(',', $domains) : '';

        return $this->httpPostJson(
            'rest/2.0/smartapp/app/modifywebviewdomain',
            ['web_view_domain' => $domains, 'action' => $action]
        );
    }
}
