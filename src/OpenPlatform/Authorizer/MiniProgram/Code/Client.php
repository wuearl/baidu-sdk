<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Baidu\OpenPlatform\Authorizer\MiniProgram\Code;

use Baidu\Kernel\BaseClient;
use  Httpbas\Kernel\Http;
use HttpBase\Http\StreamResponse;

class Client extends BaseClient
{
    /**
     * @return array|\Baidu\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function gettrial()
    {
        return $this->httpGet('rest/2.0/smartapp/package/gettrial');
    }

    /**
     * @return array|\Baidu\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function list()
    {
        return $this->httpGet('rest/2.0/smartapp/package/get');
    }

    /**
     * @param int $templateId
     * @param string $extJson
     * @param string $version
     * @param string $description
     * @return array|\Baidu\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function commit(int $templateId, string $extJson, string $version, string $description)
    {
        return $this->httpPost('rest/2.0/smartapp/package/upload', [
            'template_id' => $templateId,
            'ext_json' => $extJson,
            'user_version' => $version,
            'user_desc' => $description,
        ]);
    }

    /**
     * @param string $package_id
     * @param string $path
     * @param int $width
     * @return array|\HttpBase\Http\Response|\HttpBase\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function getQrCode(string $package_id, string $path, int $width = 200)
    {
        $params = array_filter(compact('package_id', 'path', 'width'));
        $response = $this->requestRaw('rest/2.0/smartapp/app/qrcode', 'get', ['query' => $params]);

        if (false !== stripos($response->getHeaderLine('Content-Type'), 'image/png')) {
            return StreamResponse::buildFromPsrResponse($response);
        }
        return $this->castResponseToType($response, $this->app['config']->get('response_type'));
    }

    /**
     * @return array|\Baidu\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function getCategory()
    {
        return $this->httpGet('rest/2.0/smartapp/app/category/list', ['category_type' => 2]);
    }

    /**
     * @param int $package_id
     * @param string $content
     * @param string $remark
     * @return array|\Baidu\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function submitAudit(int $package_id, string $content, string $remark)
    {
        return $this->httpPost('rest/2.0/smartapp/package/submitaudit', compact('package_id', 'content', 'remark'));
    }

    /**
     * @param string $package_id
     * @return array|\Baidu\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function release(string $package_id)
    {
        return $this->httpPost('rest/2.0/smartapp/package/release', compact('package_id'));
    }
}