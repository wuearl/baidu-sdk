<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Baidu\OpenPlatform\Authorizer\MiniProgram;

use Baidu\MiniProgram\Application as MiniProgram;

/**
 * Class Application.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 *
 * @property \Baidu\OpenPlatform\Authorizer\MiniProgram\Account\Client $account
 * @property \Baidu\OpenPlatform\Authorizer\MiniProgram\Code\Client $code
 * @property \Baidu\OpenPlatform\Authorizer\MiniProgram\Domain\Client $domain
 */
class Application extends MiniProgram
{
    public function __construct(array $config = [], array $prepends = [])
    {
        parent::__construct($config, $prepends);

        $providers = [
            Code\ServiceProvider::class,
            Domain\ServiceProvider::class,
            Account\ServiceProvider::class,
            Auth\ServiceProvider::class
        ];

        foreach ($providers as $provider) {
            $this->register(new $provider());
        }
    }
}
