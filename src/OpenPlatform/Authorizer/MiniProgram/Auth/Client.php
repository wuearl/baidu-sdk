<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Baidu\OpenPlatform\Authorizer\MiniProgram\Auth;

use Baidu\Kernel\BaseClient;


class Client extends BaseClient
{
    /**
     *  Get session info by code.
     * @param string $code
     * @return array|\Baidu\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function session(string $code)
    {
        $params = [
            'code' => $code,
            'grant_type' => 'authorization_code'
        ];

        return $this->httpGet('rest/2.0/oauth/getsessionkeybycode', $params);
    }
}
