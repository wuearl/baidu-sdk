<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Baidu\OpenPlatform\Authorizer\Auth;

use Baidu\Kernel\AccessToken as BaseAccessToken;
use Baidu\Kernel\Support\Arr;
use Baidu\Kernel\Support\Collection;
use Baidu\Kernel\Traits\Observable;
use HttpBase\Exceptions\HttpException;
use Pimple\Container;
use Baidu\OpenPlatform\Application;

/**
 * Class AccessToken.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class AccessToken extends BaseAccessToken
{
    use Observable;

    const EVENT_ACCESS_TOKEN = 'access_token';

    public function __construct(Container $app, Application $component)
    {
        parent::__construct($app);

        $this->component = $component;
    }

    /**
     * {@inheritdoc}.
     */
    protected $tokenKey = 'access_token';

    /**
     * @var \Baidu\OpenPlatform\Application
     */
    protected $component;

    /**
     * {@inheritdoc}.
     */
    protected function getCredentials(): array
    {
        return [
            'refresh_token' => $this->app['config']['refresh_token'],
            'grant_type' => 'app_to_tp_refresh_token'
        ];
    }

    public function requestToken(array $credentials, $toArray = false)
    {
        $result = $formatted = $this->component->getAuthorizerToken($credentials);

        if ($formatted instanceof Collection) {
            $result = $formatted->toArray();
        }
        if (empty($result[$this->tokenKey])) {
            throw new HttpException('Request access_token fail: ' . json_encode($result, JSON_UNESCAPED_UNICODE));
        }

        $this->dispatch($this->tokenKey, $result);

        return $toArray ? $result : $formatted;
    }

    /**
     * @return string
     */
    protected function getCacheKey()
    {
        return $this->cachePrefix . md5(json_encode(array_merge(['authorizer_appid' => $this->app['config']['app_id']], Arr::except($this->getCredentials(), ['refresh_token']))));
    }
}
