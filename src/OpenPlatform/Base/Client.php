<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Baidu\OpenPlatform\Base;

use Baidu\Kernel\BaseClient;

/**
 * Class Client.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Client extends BaseClient
{
    /**
     * Get authorization info.
     * @param string|null $authCode
     * @return array|\Baidu\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function handleAuthorize(string $authCode = null)
    {
        $params = [
            'code' => $authCode ?? $this->app['request']->get('auth_code'),
            'grant_type' => 'app_to_tp_authorization_code'
        ];

        return $this->httpGet('rest/2.0/oauth/token', $params);
    }

    /**
     * Get authorizer access_token.
     * @param array $credentials
     * @return array|\Baidu\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function getAuthorizerToken(array $credentials)
    {
        return $this->httpGet('rest/2.0/oauth/token', $credentials);
    }

    /**
     * Create pre-authorization code.
     * @return array|\Baidu\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function createPreAuthorizationCode()
    {
        return $this->httpGet('rest/2.0/smartapp/tp/createpreauthcode');
    }
}
